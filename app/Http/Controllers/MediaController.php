<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MediaController extends Controller
{
    public $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function uploadPhoto()
    {
        if ($this->request->has('upload') && $this->request->upload != '') {
            $slug = 'image';

            $imageUrl = upload_image($this->request->upload, $slug);
            $imageName = substr($imageUrl, 9, strlen($imageUrl));

            return response()->json([
                "uploaded" => 1,
                "fileName" => $imageName,
                "url" => $imageUrl
            ]);
        }

        return response()->json([
            "uploaded" => 0,
            "fileName" => '',
            "url" => ''
        ]);
    }
}
