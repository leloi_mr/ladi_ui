<?php

namespace App\Http\Controllers;

use App\Models\Food;
use App\Models\MealKit;
use App\Models\Recipe;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class FoodController extends Controller
{
    public $request, $food, $mealKit, $recipe;

    public function __construct(Request $request, Food $food, MealKit $mealKit, Recipe $recipe)
    {
        $this->request = $request;
        $this->food = $food;
        $this->mealKit = $mealKit;
        $this->recipe = $recipe;
    }

    public function index()
    {
        if ($this->request->ajax() && $this->request->has('ajax')) {
            return $this->datatable();
        }

        return view('food.index');
    }

    public function datatable()
    {
        return DataTables::of(
            $this->food->query()
                ->with('mealKit')
                ->with('recipe')
                ->get()
        )
            ->addIndexColumn()
            ->addColumn('mealKitName', function ($row) {
                return $row->mealKit ? $row->mealKit->title : '';
            })
            ->addColumn('typeName', function ($row) {
                return $row->getTypeName() ? $row->getTypeName() : '';
            })
            ->addColumn('is_in_week_menu_switch', function ($row) {
                $col = 'is_in_week_menu';
                $checked = ($row[$col] === 1 ? ' checked' : '');
                $res = '<div class="bt-switch" data-remote="' . route('food.is_in_week_menu', $row->id) . '"
                data-column="' . $col . '" data-name="' . __('food.is_in_week_menu') . '"><input type="checkbox"' . $checked . ' data-id="' . $row['id'] . '"
                data-on-color="success" data-off-color="info" data-on-text="' . __('food.is_in_week_menu_on') . '" data-off-text="' . __('food.is_in_week_menu_off') . '" data-width="100%"></div>';

                return $res;
            })
            ->addColumn('action', function ($row) {
                $btn = '<a href="' . route('food.edit', ['id' => $row->id]) . '"
                        class="btn btn-primary btn-sm">' . __('web.edit') . '</a>';

                $btn .= '<a href="#" data-remote="' . route('food.destroy', ['id' => $row->id]) . '"
                        class="btn btn-dark btn-sm btn-delete text-white">' . __('web.delete') . '</a>';
                return $btn;
            })
            ->rawColumns(['action', 'is_in_week_menu_switch'])
            ->make(true);
    }

    public function isInWeekMenu($id)
    {
        $value = $this->request->is_in_week_menu;
        if ($value == 'true') $value = 1;
        elseif ($value == 'false') $value = 0;
        else {
            return response()->json([
                'status' => 'error'
            ]);
        }

        try {
            $this->food->findOrFail($id)->update([
                'is_in_week_menu' => $value
            ]);

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function create()
    {
        $listMealKit = $this->mealKit->all();
        $listTypeArray = $this->food::getTypeArrayValue();
        $typeDonLe = $this->mealKit::TYPE_DON_LE;
        $listRecipe = $this->recipe->all();

        return view('food.create')
            ->with([
                'listMealKit' => $listMealKit,
                'listTypeArray' => $listTypeArray,
                'typeDonLe' => $typeDonLe,
                'listRecipe' => $listRecipe,
            ]);
    }

    public function store()
    {
        $this->request->merge(['is_special' => $this->request->is_special === 'on' ? Food::IS_SPECIAL_TRUE : Food::IS_SPECIAL_FALSE]);
        $this->request->merge(['is_in_week_menu' => $this->request->is_in_week_menu === 'on' ? Food::IS_IN_WEEK_MENU_TRUE : Food::IS_IN_WEEK_MENU_FALSE]);

        $inputs = $this->request->all();
        $inputs['title'] = clearXSS($this->request->title);
        $inputs['content'] = clearXSS($this->request->content);
        $inputs['meal_kit_id'] = clearXSS($this->request->meal_kit_id);
        $inputs['type'] = clearXSS($this->request->type);
        $inputs['image'] = $this->request->file('image');
        $inputs['is_special'] = clearXSS($this->request->is_special);
        $inputs['price_plus'] = clearXSS($this->request->price_plus);
        $inputs['is_in_week_menu'] = clearXSS($this->request->is_in_week_menu);
        $inputs['recipe_id'] = clearXSS($this->request->recipe_id);

        $validator = Validator::make($inputs, [
            'title' => 'required|string|max:255',
            'content' => 'required|string|max:10000',
            'image' => 'nullable|mimes:jpeg,jpg,png,gif|max:10240',
            'meal_kit_id' => 'required|exists:' . $this->mealKit->getTable() . ',id',
            'type' => 'nullable|in:' . implode(',', $this->food->getTypeArray()),
            'is_special' => 'required|in:' . implode(',', $this->food->getIsSpecialArray()),
            'price_plus' => 'nullable|numeric|min:0',
            'is_in_week_menu' => 'required|in:' . implode(',', $this->food->getIsInWeekMenuArray()),
            'recipe_id' => 'nullable|exists:' . $this->recipe->getTable() . ',id',
        ]);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $slug = 'food';
            if ($this->request->has('image') && $this->request->file('image')) {
                $this->request->image = upload_image($this->request->file('image'), $slug);
            }
            $this->food->createFood($this->request);

            return redirect()->route('food.index')
                ->with('success', __('web.create-successed', ['name' => __('food.index')]));
        } catch (\Exception $e) {
            return back()
                ->with('exception', $e->getMessage())
                ->with('error', __('web.create-errored', ['name' => __('food.index')]))
                ->withInput();
        }
    }

    public function edit($id)
    {
        $food = $this->food->findOrFail($id);
        $listMealKit = $this->mealKit->all();
        $listTypeArray = $this->food::getTypeArrayValue();
        $typeDonLe = $this->mealKit::TYPE_DON_LE;
        $listRecipe = $this->recipe->all();

        return view('food.edit')
            ->with([
                'food' => $food,
                'listMealKit' => $listMealKit,
                'listTypeArray' => $listTypeArray,
                'typeDonLe' => $typeDonLe,
                'listRecipe' => $listRecipe,
            ]);
    }

    public function update($id)
    {
        $this->request->merge(['is_special' => $this->request->is_special === 'on' ? Food::IS_SPECIAL_TRUE : Food::IS_SPECIAL_FALSE]);
        $this->request->merge(['is_in_week_menu' => $this->request->is_in_week_menu === 'on' ? Food::IS_IN_WEEK_MENU_TRUE : Food::IS_IN_WEEK_MENU_FALSE]);

        $inputs = $this->request->except(['is_no_action_image']);
        $inputs['title'] = clearXSS($this->request->title);
        $inputs['content'] = clearXSS($this->request->content);
        $inputs['meal_kit_id'] = clearXSS($this->request->meal_kit_id);
        $inputs['type'] = clearXSS($this->request->type);
        $inputs['image'] = $this->request->file('image');
        $inputs['is_special'] = clearXSS($this->request->is_special);
        $inputs['price_plus'] = clearXSS($this->request->price_plus);
        $inputs['is_in_week_menu'] = clearXSS($this->request->is_in_week_menu);
        $inputs['recipe_id'] = clearXSS($this->request->recipe_id);

        $validator = Validator::make($inputs, [
            'title' => 'required|string|max:255',
            'content' => 'required|string|max:10000',
            'image' => 'nullable|mimes:jpeg,jpg,png,gif|max:10240',
            'meal_kit_id' => 'required|exists:' . $this->mealKit->getTable() . ',id',
            'type' => 'nullable|in:' . implode(',', $this->food->getTypeArray()),
            'is_special' => 'required|in:' . implode(',', $this->food->getIsSpecialArray()),
            'price_plus' => 'nullable|numeric|min:0',
            'is_in_week_menu' => 'required|in:' . implode(',', $this->food->getIsInWeekMenuArray()),
            'recipe_id' => 'nullable|exists:' . $this->recipe->getTable() . ',id',
        ]);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $slug = 'food';
            $foodObj = $this->food->findOrFail($id);

            $this->request->image = $foodObj->image;
            if ($this->request->has('image') && $this->request->file('image')) {
                $this->request->image = upload_image($this->request->file('image'), $slug);
            } else if ($this->request->has('is_no_action_image') && $this->request->input('is_no_action_image') == 'false') {
                $this->request->image = null;
            }

            $foodObj->editFood($this->request);

            return redirect()->route('food.index')
                ->with('success', __('web.edit-successed', ['name' => __('food.index')]));
        } catch (\Exception $e) {
            return back()
                ->with('exception', $e->getMessage())
                ->with('error', __('web.edit-errored', ['name' => __('food.index')]))
                ->withInput();
        }
    }

    public function destroy($id)
    {
        try {
            $this->food->findOrFail($id)->delete();

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
}
