<?php

namespace App\Http\Controllers;

use App\Models\Food;
use App\Models\MealKit;
use App\Models\Recipe;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MealkitController extends Controller
{
    public $request, $mealkit;

    public function __construct(Request $request, MealKit $mealKit)
    {
        $this->request = $request;
        $this->mealkit = $mealKit;
    }

    public function index()
    {
        if ($this->request->ajax() && $this->request->has('ajax')) {
            return $this->datatable();
        }

        return view('mealkit.index');
    }

    public function datatable()
    {
        return DataTables::of(
            $this->mealkit->query()
                ->get()
        )
            ->addIndexColumn()
            ->addColumn('typeName', function ($row) {
                return $row->getTypeName() ? $row->getTypeName() : '';
            })
            ->addColumn('action', function ($row) {
                $btn = '<a href="' . route('mealkit.edit', ['id' => $row->id]) . '"
                        class="btn btn-primary btn-sm">' . __('web.edit') . '</a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function edit($id)
    {
        $mealkit = $this->mealkit->findOrFail($id);
        $listMealKit = $this->mealkit->all();
        $listTypeArray = $this->mealkit::getTypeArrayValue();
        $typeDonLe = $this->mealkit::TYPE_DON_LE;

        return view('mealkit.edit')
            ->with([
                'mealkit' => $mealkit,
                'listMealKit' => $listMealKit,
                'listTypeArray' => $listTypeArray,
                'typeDonLe' => $typeDonLe,
            ]);
    }

    public function update($id)
    {
        $inputs = $this->request->except(['is_no_action_image']);
        $inputs['title'] = clearXSS($this->request->title);
        $inputs['image'] = $this->request->file('image');
        $inputs['alias'] = clearXSS($this->request->alias);
        $inputs['order'] = clearXSS($this->request->order);
        $inputs['description'] = clearXSS($this->request->description);
        $inputs['type'] = clearXSS($this->request->type);

        $validator = Validator::make($inputs, [
            'title' => 'required|string|max:255',
            'image' => 'nullable|mimes:jpeg,jpg,png,gif|max:10240',
            'alias' => 'required|string|max:255',
            'order' => 'required|numeric|min:0',
            'description' => 'nullable|string|max:10000',
            'type' => 'nullable|in:' . implode(',', $this->mealkit->getTypeArray()),
        ]);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $slug = 'mealkit';
            $mealkitObj = $this->mealkit->findOrFail($id);

            $this->request->image = $mealkitObj->image;
            if ($this->request->has('image') && $this->request->file('image')) {
                $this->request->image = upload_image($this->request->file('image'), $slug);
            } else if ($this->request->has('is_no_action_image') && $this->request->input('is_no_action_image') == 'false') {
                $this->request->image = null;
            }

            $mealkitObj->editMealkit($this->request);

            return redirect()->route('mealkit.index')
                ->with('success', __('web.edit-successed', ['name' => __('mealkit.index')]));
        } catch (\Exception $e) {
            return back()
                ->with('exception', $e->getMessage())
                ->with('error', __('web.edit-errored', ['name' => __('mealkit.index')]))
                ->withInput();
        }
    }
}
