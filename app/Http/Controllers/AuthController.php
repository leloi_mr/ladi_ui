<?php

namespace App\Http\Controllers;

use App\Models\UserAccessToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public $request, $userAccessToken;

    public function __construct(Request $request, UserAccessToken $userAccessToken)
    {
        $this->request = $request;
        $this->userAccessToken = $userAccessToken;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::check()) {
            return redirect()->route('home');
        }

        if ($this->request->has('token')) {
            $token = $this->request->token;

            $checkToken = $this->userAccessToken->query()->where('access_token', $token)->first();
            if ($token == '123') { // for test, use '123'
                $checkToken = $this->userAccessToken->query()->where('user_id', 1)->first();
            }

            if ($checkToken) {
                $userId = $checkToken->user_id;
                $userId && Auth::loginUsingId($userId);

                if (Auth::check()) {
                    return redirect()->route('home');
                }
            }
        }

        return view('welcome');
    }
}
