<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use PHPHtmlParser\Dom;

if (!function_exists('clearXSS')) {
    /**
     * @param  $file
     *
     * @return string
     */
    function clearXSS($string)
    {
        $string = nl2br($string);
        $string = trim(strip_tags($string));
        $string = removeScripts($string);

        return $string;
    }
}

if (!function_exists('removeScripts')) {
    function removeScripts($str)
    {
        $regex =
            '/(<link[^>]+rel="[^"]*stylesheet"[^>]*>)|' .
            '<script[^>]*>.*?<\/script>|' .
            '<style[^>]*>.*?<\/style>|' .
            '<!--.*?-->/is';

        return preg_replace($regex, '', $str);
    }
}

if (!function_exists('word_limiter')) {
    function word_limiter($str, $limit = 100, $end_char = '&#8230;')
    {
        if (trim($str) == '') {
            return $str;
        }
        preg_match('/^\s*+(?:\S++\s*+){1,' . (int) $limit . '}/', $str, $matches);
        if (strlen($str) == strlen($matches[0])) {
            $end_char = '';
        }
        return rtrim($matches[0]) . $end_char;
    }
}

if (!function_exists('character_limiter')) {
    function character_limiter($str, $n = 500, $end_char = '&#8230;')
    {

        $str = preg_replace("/\\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));
        if (strlen($str) <= $n) {
            return $str;
        }
        $out = "";
        foreach (explode(' ', trim($str)) as $val) {
            $out .= $val . ' ';
            if (strlen($out) >= $n) {
                $out = trim($out);
                return (strlen($out) == strlen($str)) ? $out : $out . $end_char;
            }
        }
    }
}
