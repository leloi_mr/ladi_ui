<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MealKit extends Model
{
    use SoftDeletes;
    protected $table = 'meal_kit';

    const TYPE_DEFAULT = 1;
    const TYPE_DON_LE = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'image', 'alias', 'order', 'description', 'type'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'login_date' => 'datetime',
        // 'expired_date' => 'datetime',
    ];

    protected $appends = [
        'image_url'
    ];

    public function getTypeName()
    {
        return self::getTypeValue($this->type);
    }

    public static function getTypeValue($type)
    {
        switch ($type) {
            case self::TYPE_DEFAULT:
                return 'Set nhiều loại món ăn';
            case self::TYPE_DON_LE:
                return 'Set đơn lẻ';
        }
        return null;
    }

    public static function getTypeArray()
    {
        return [
            self::TYPE_DEFAULT,
            self::TYPE_DON_LE,
        ];
    }

    public static function getTypeArrayValue()
    {
        $res = [];
        foreach (self::getTypeArray() as $type) {
            array_push($res, [
                'value' => $type,
                'name' => self::getTypeValue($type)
            ]);
        }
        return $res;
    }

    public function editMealkit($request)
    {
        DB::beginTransaction();

        try {
            $data = $this->prepareEditData([
                'title' => $request->title,
                'image' => $request->image,
                'alias' => $request->alias,
                'order' => $request->order,
                'description' => $request->description,
                'type' => $request->type,
            ]);
            $this->update($data);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    public function prepareEditData($data)
    {
        $data = array_filter($data, function ($v, $k) {
            return $v || $k == 'image';
        }, ARRAY_FILTER_USE_BOTH);
        return $data;
    }

    public function getImageUrlAttribute()
    {
        return $this->image;
    }
}
