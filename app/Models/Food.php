<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Food extends Model
{
    use SoftDeletes;
    protected $table = 'food';

    const TYPE_CHINH = 1;
    const TYPE_CANH = 2;
    const TYPE_RAU = 3;
    const TYPE_DON_LE = 4;

    const IS_SPECIAL_TRUE = 1;
    const IS_SPECIAL_FALSE = 2;

    const IS_IN_WEEK_MENU_TRUE = 1;
    const IS_IN_WEEK_MENU_FALSE = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'image', 'content', 'meal_kit_id', 'type', 'is_special', 'price_plus', 'is_in_week_menu', 'recipe_id'
    ];

    protected $appends = [
        'image_url', 'is_special_name', 'is_in_week_menu_name'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'login_date' => 'datetime',
        // 'expired_date' => 'datetime',
    ];

    public function mealKit()
    {
        return $this->belongsTo(MealKit::class, 'meal_kit_id');
    }

    public function recipe()
    {
        return $this->belongsTo(MealKit::class, 'meal_kit_id');
    }

    public function getTypeName()
    {
        if ($this->mealKit && $this->mealKit->type === MealKit::TYPE_DON_LE) {
            return self::getTypeValue(self::TYPE_DON_LE);
        }

        return self::getTypeValue($this->type);
    }

    public static function getTypeValue($type)
    {
        switch ($type) {
            case self::TYPE_CHINH:
                return 'Món chính';
            case self::TYPE_CANH:
                return 'Món canh';
            case self::TYPE_RAU:
                return 'Món rau';
            case self::TYPE_DON_LE:
                return 'Món đơn lẻ';
        }
        return null;
    }

    public static function getTypeArray()
    {
        return [
            self::TYPE_CHINH,
            self::TYPE_CANH,
            self::TYPE_RAU
        ];
    }

    public static function getTypeArrayValue()
    {
        $res = [];
        foreach (self::getTypeArray() as $type) {
            array_push($res, [
                'value' => $type,
                'name' => self::getTypeValue($type)
            ]);
        }
        return $res;
    }

    public static function getIsSpecialArray()
    {
        return [
            self::IS_SPECIAL_TRUE,
            self::IS_SPECIAL_FALSE
        ];
    }

    public function getIsSpecialNameAttribute()
    {
        return self::getIsSpecialArrayValue($this->is_special);
    }

    public static function getIsSpecialArrayValue($isSpecial)
    {
        switch ($isSpecial) {
            case self::IS_SPECIAL_TRUE:
                return 'on';
            case self::IS_SPECIAL_FALSE:
                return 'off';
        }
        return null;
    }

    public static function getIsInWeekMenuArray()
    {
        return [
            self::IS_IN_WEEK_MENU_TRUE,
            self::IS_IN_WEEK_MENU_FALSE
        ];
    }

    public function getIsInWeekMenuNameAttribute()
    {
        return self::getIsInWeekMenuArrayValue($this->is_in_week_menu);
    }

    public static function getIsInWeekMenuArrayValue($isInWeekMenu)
    {
        switch ($isInWeekMenu) {
            case self::IS_IN_WEEK_MENU_TRUE:
                return 'on';
            case self::IS_IN_WEEK_MENU_FALSE:
                return 'off';
        }
        return null;
    }

    public function createFood($request)
    {
        DB::beginTransaction();

        try {
            $data = $this->prepareCreateData([
                'title' => $request->title,
                'image' => $request->image,
                'content' => $request->content,
                'meal_kit_id' => $request->meal_kit_id,
                'type' => $request->type,
                'is_special' => $request->is_special,
                'price_plus' => $request->price_plus,
                'is_in_week_menu' => $request->is_in_week_menu,
                'recipe_id' => $request->recipe_id
            ]);

            $this->create($data);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    public function prepareCreateData($data)
    {
        $data = array_filter($data, function ($v, $k) {
            return $v;
        }, ARRAY_FILTER_USE_BOTH);
        return $data;
    }

    public function editFood($request)
    {
        DB::beginTransaction();

        try {
            $data = $this->prepareEditData([
                'title' => $request->title,
                'image' => $request->image,
                'content' => $request->content,
                'meal_kit_id' => $request->meal_kit_id,
                'type' => $request->type,
                'is_special' => $request->is_special,
                'price_plus' => $request->price_plus,
                'is_in_week_menu' => $request->is_in_week_menu,
                'recipe_id' => $request->recipe_id
            ]);
            $this->update($data);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    public function prepareEditData($data)
    {
        $data = array_filter($data, function ($v, $k) {
            return $v || $k == 'image';
        }, ARRAY_FILTER_USE_BOTH);
        return $data;
    }

    public function getImageUrlAttribute()
    {
        return $this->image;
    }
}
