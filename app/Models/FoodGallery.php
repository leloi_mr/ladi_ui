<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodGallery extends Model
{
    use SoftDeletes;
    protected $table = 'food_gallery';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'food_id', 'image', 'order'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'login_date' => 'datetime',
        // 'expired_date' => 'datetime',
    ];
}
