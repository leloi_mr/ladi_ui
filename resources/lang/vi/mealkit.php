<?php

return [

    'index' => 'Set món ăn',
    'index.title' => 'Set món ăn',
    'title' => 'Tiêu đề',
    'image' => 'Hình ảnh',
    'image_size_suggest' => '',
    'alias' => 'Tiêu đề rút gọn',
    'alias_suggest' => '(Hiển thị tại các chú thích)',
    'order' => 'Vị trí',
    'type' => 'Loại set món ăn',
    'description' => 'Mô tả',
];
