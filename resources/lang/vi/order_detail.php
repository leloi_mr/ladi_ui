<?php

return [

    'index' => 'Chi tiết đơn hàng',
    'index.title' => 'Chi tiết đơn hàng',

    'order_id' => 'Đơn hàng',
    'type' => 'Loại đơn hàng',
    'meal_kit_id' => 'Meal kit',
    'product_id' => 'Sản phẩm',
    'meal_kit_content' => 'Thông tin Meal kit',
    'food' => 'Chi tiết đơn hàng',
    'price' => 'Giá',
    'quantity' => 'Số lượng',
    'unit' => 'Đơn vị'
];
