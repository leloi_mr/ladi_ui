<?php

return [

    'index' => 'Món ăn',
    'index.title' => 'Món ăn',

    'title' => 'Tiêu đề',
    'image' => 'Hình ảnh',
    'image_size_suggest' => '',
    'content' => 'Mô tả',
    'meal_kit_id' => 'Set món ăn',
    'meal_kit_name' => 'Set món ăn',
    'type' => 'Loại món ăn',
    'is_special' => 'Sử dụng nguyên liệu đặc sản',
    'is_special_on' => 'Có',
    'is_special_off' => 'Không',
    'price_plus' => 'Giá cộng thêm',
    'price_plus_unit' => 'vnđ/suất',
    'is_in_week_menu' => 'Món ăn trong tuần này',
    'is_in_week_menu_on' => 'Có',
    'is_in_week_menu_off' => 'Không',
    'recipe_id' => 'Công thức',
    'recipe_name' => 'Công thức nấu ăn',
];
