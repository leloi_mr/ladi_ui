<?php

return [

    'index' => 'Đơn hàng',
    'index.title' => 'Đơn hàng',
    'customer_id' => 'Khách hàng',
    'booking_date' => 'Ngày đặt hàng',
    'total_price' => 'Tổng giá',
    'ship_price' => 'Giá vận chuyển',
    'voucher_id' => 'Thẻ giảm giá',
    'status' => 'Trạng thái',
    'address' => 'Địa chỉ',
    'code' => 'Mã đơn hàng',
    'note' => 'Ghi chú',
    'title' => 'Tiêu đề'
];
