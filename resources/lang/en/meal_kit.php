<?php

return [

    'index' => 'Set món ăn',
    'index.title' => 'Set món ăn',
    'title' => 'Tiêu đề',
    'image' => 'Hình ảnh',
    'alias' => 'Tiêu đề rút gọn',
    'order' => 'Vị trí',
    'description' => 'Mô tả',
];
