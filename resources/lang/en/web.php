<?php

return [

    "create" => "Thêm mới",
    "add" => "Thêm mới",
    "edit" => "Sửa",
    "delete" => "Xóa",
    "save" => "Lưu",

    "success" => "Thành công.",
    "error" => "Thất bại.",

    "create-success" => "Thêm mới thành công.",
    "edit-success" => "Chỉnh sửa thành công.",
    "delete-success" => "Xóa thành công.",
    "save-success" => "Lưu thành công.",

    "create-successed" => "Thêm mới :name thành công.",
    "edit-successed" => "Chỉnh sửa :name thành công.",
    "delete-successed" => "Xóa :name thành công.",
    "save-successed" => "Lưu :name thành công.",

    "upload" => "Tải lên",
    "upload-successed" => "Tải lên :name thành công.",

    "create-error" => "Thêm mới thất bại.",
    "edit-error" => "Chỉnh sửa thất bại.",
    "delete-error" => "Xóa thất bại.",
    "save-error" => "Lưu thất bại.",

    "create-errored" => "Thêm mới :name thất bại.",
    "edit-errored" => "Chỉnh sửa :name thất bại.",
    "delete-errored" => "Xóa :name thất bại.",
    "save-errored" => "Lưu :name thất bại.",

    "upload-errored" => "Tải lên :name thất bại.",

    "confirm-delete" => "Bạn xác nhận muốn Xóa?",
    "confirm-delete-detail" => "Bạn sẽ không thể khôi phục lại thao tác này!",
    "accept" => "Đồng ý",
    "cancel" => "Hủy bỏ",
    "deleted" => "Đã xóa",
    "cancelled" => "Đã hủy",

    "not-found" => "Không tìm thấy :name",
    "invalid-image-type" => "Sai định dạng ảnh cho phép, chỉ chấp nhận: JPG, JPEG, GIF, PNG",
    "max-upload-album" => "Kích thước tải lên tối đa là : :max . Vui lòng chọn tập tin có kích thước nhỏ hơn.",

    "action" => "Hành động",
    "more-action" => "Thêm",

    "creating" => "Tạo mới :name",
    "editing" => "Chỉnh sửa :name",

    "actived" => "Đã kích hoạt",
    "in-actived" => "Chưa kích hoạt",

    "select-an-option" => "Chọn một lựa chọn từ danh sách",
    "something-went-wrong" => "Có lỗi xảy ra.",

    "table.no" => "#",

];
