@extends('layouts.admin')

@section('title', __('web.creating', ['name' => __('food.index')]))

@push('after-styles')
{{ Html::style('xtreme-admin/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') }}
<style>
    #preview_image,
    #preview_image_seo {
        max-width: 150px;
        height: auto;
    }
</style>
@endpush

@section('content')
{{ Breadcrumbs::render('food.create') }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body mx-5">
            <form class="mt-3" action="{{ route('food.store') }}" method="post" enctype="multipart/form-data"
                id="the_form">
                @csrf

                <h3 class="card-title text-center">{{ __('web.creating', ['name' => __('food.index')]) }}</h3>
                <div class="form-body">
                    <div class="card-body pt-0">
                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="title">{{ __('food.title') }} <span
                                            class="text-danger">(*)</span></label>
                                    <input id="title" type="text" data-name-show="{{ __('food.title') }}"
                                        class="custom-validate form-control form-control-lg @error('title') is-invalid @enderror"
                                        name="title" value="{{ old('title') }}" autofocus maxlength="255" required>

                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="image" class="control-label">{{ __('food.image') }}
                                        {{ __('food.image_size_suggest') }}</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">{{ __('web.upload') }}</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" accept="image/*"
                                                class="custom-file-input form-control form-control-lg @error('image') is-invalid @enderror"
                                                name="image" value=""
                                                onchange="document.getElementById('output_image').src = window.URL.createObjectURL(this.files[0]);">
                                            <label class="custom-file-label" for="inputGroupFile01">{{ __('web.choose-file') }}</label>
                                        </div>
                                    </div>
                                    <div class="card mt-2" id="preview_image">
                                        <img id="output_image" alt="{{ __('food.image') }}"
                                            class="img-thumbnail" src="#" />
                                        <div class="card-action position-relative">
                                            <button class="btn btn-danger position-absolute"
                                                style="bottom: 10px; right: 10px;" id="remove_image"><i
                                                    class="ti-trash"></i></button>
                                        </div>
                                    </div>

                                    @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="content">{{ __('food.content') }} <span
                                            class="text-danger">(*)</span></label>
                                    <textarea id="content" data-name-show="{{ __('food.content') }}"
                                        class="custom-validate form-control form-control-lg @error('content') is-invalid @enderror"
                                        name="content" maxlength="255" required>{{ old('content') }}</textarea>

                                    @error('content')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="invalid-feedback d-none" id="validate_content" role="alert">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="meal_kit_id">{{ __('food.meal_kit_id') }} <span
                                            class="text-danger">(*)</span></label>
                                    <select name="meal_kit_id" id="meal_kit_id"
                                        data-name-show="{{ __('food.meal_kit_id') }}"
                                        class="select2 custom-validate form-control form-control-lg @error('meal_kit_id') is-invalid @enderror"
                                        required>
                                        <option value="" disabled selected>{{ __('web.select-an-option') }}</option>

                                        @foreach ($listMealKit as $mealKit)
                                        <option value="{{ $mealKit->id }}"
                                            {{ old('meal_kit_id') == $mealKit->id ? ' selected' : '' }}
                                            data-type="{{ $mealKit->type}}">
                                            {{ $mealKit->title }}
                                        </option>
                                        @endforeach
                                    </select>

                                    @error('meal_kit_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6" id="form_type">
                                <div class="form-group">
                                    <label class="control-label" for="type">{{ __('food.type') }} <span
                                            class="text-danger">(*)</span></label>
                                    <select name="type" id="type" data-name-show="{{ __('food.type') }}"
                                        class="select2 custom-validate form-control form-control-lg @error('type') is-invalid @enderror"
                                        required>
                                        <option value="" disabled selected>{{ __('web.select-an-option') }}</option>

                                        @foreach($listTypeArray as $typeArray)
                                        <option value="{{ $typeArray['value'] }}"
                                            {{ old('type') == $typeArray['value'] ? ' selected' : '' }}>
                                            {{ $typeArray['name'] }}
                                        </option>
                                        @endforeach
                                    </select>

                                    @error('type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row pt-3">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" for="is_special">{{ __('food.is_special') }}
                                        <span class="text-danger">(*)</span>
                                    </label>
                                    <div class="bt-switch">
                                        <input type="checkbox" data-on-color="success" data-off-color="info"
                                            name="is_special" id="is_special"
                                            data-on-text="{{ __('food.is_special_on') }}"
                                            data-off-text="{{ __('food.is_special_off') }}">
                                    </div>

                                    @error('is_special')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3" id="form-price-plus">
                                <div class="form-group">
                                    <label class="control-label" for="price_plus">{{ __('food.price_plus') }} <span
                                            class="text-danger">(*)</span></label>
                                    <div class="row">
                                        <input id="price_plus" type="number"
                                            data-name-show="{{ __('food.price_plus') }}"
                                            class="col-md-6 custom-validate form-control form-control-lg @error('price_plus') is-invalid @enderror"
                                            name="price_plus" value="{{ old('price_plus') ? old('price_plus') : 0 }}"
                                            autofocus min="0" required>
                                        <div class="col-md-6 text-left d-flex" style="align-items: center;">
                                            <span>{{ __('food.price_plus_unit')}}</span></div>
                                    </div>

                                    @error('price_plus')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row pt-3">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" for="is_in_week_menu">{{ __('food.is_in_week_menu') }}
                                        <span class="text-danger">(*)</span>
                                    </label>
                                    <div class="bt-switch">
                                        <input type="checkbox" checked data-on-color="success" data-off-color="info"
                                            name="is_in_week_menu" id="is_in_week_menu"
                                            data-on-text="{{ __('food.is_in_week_menu_on') }}"
                                            data-off-text="{{ __('food.is_in_week_menu_off') }}">
                                    </div>

                                    @error('is_in_week_menu')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row pt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="recipe_id">{{ __('food.recipe_name') }} <span
                                            class="text-danger"></span></label>
                                    <select name="recipe_id" id="recipe_id"
                                        data-name-show="{{ __('food.recipe_name') }}"
                                        class="select2 custom-validate form-control form-control-lg @error('recipe_id') is-invalid @enderror">
                                        <option value="" disabled selected>{{ __('web.select-an-option') }}</option>

                                        @foreach ($listRecipe as $recipe)
                                        <option value="{{ $recipe->id }}"
                                            {{ old('recipe_id') == $recipe->id ? ' selected' : '' }}>
                                            {{ $recipe->name }}
                                        </option>
                                        @endforeach
                                    </select>

                                    @error('recipe_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                    <div class="row form-group mt-2 ml-1">
                                        <span>Hoặc truy cập <a href="{{ route('recipes.index') }}">danh mục công
                                                thức</a> để tạo công thức mới</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="card-body text-center">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>
                                    {{ __('web.save') }}</button>
                            </div>
                        </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('after-scripts')
{!! Html::script('xtreme-admin/assets/libs/ckeditor/ckeditor.js') !!}
{!! Html::script('xtreme-admin/assets/libs/bootstrap-switch/dist/js/bootstrap-switch.min.js') !!}

<script>
    $(document).ready(function () {
            CKEDITOR.config.filebrowserImageUploadUrl = '{!! route("ckEditorUploadPhoto")."?_token=".csrf_token() !!}';
            CKEDITOR.config.height = '300px';
            CKEDITOR.replace(document.querySelector( '#content' ));

            $.each($('.select2'), function (i, e) {
                $(e).select2({
                    placeholder: '{{ __("web.select-an-option") }}'
                });
            });

            $(".bt-switch input[type='checkbox'], .bt-switch input[type='radio']").bootstrapSwitch();
            $('#preview_image').hide();

            listenValidateForm();
            $('button[type=submit]').click(function (e) {
                if (CKEDITOR.instances.content.getData() == '') {
                    var msg = '{!! __('validation.required', ['attribute' => __('food.content')]) !!}';
                    addErrorValidateForm($('#validate_content'), msg);
                }

                validateForm(e);
            });

            function initFormType() {
                var value = $("#meal_kit_id option:selected").data('type');
                if (value == {{ $typeDonLe}}) {
                    $('#form_type').hide();
                    $('#type').removeAttr('required');
                } else {
                    $('#type').attr('required', 'required');
                    $('#form_type').show();
                }
            }
            initFormType();

            $('#meal_kit_id').on('change', function (e) {
                initFormType();
            });

            function initFormSpecial() {
                var value = $("#is_special").is(":checked");
                if (value) {
                    $('#form-price-plus').show();
                } else {
                    $('#form-price-plus').hide();
                }
            }
            initFormSpecial();

            $('#is_special').on('switchChange.bootstrapSwitch', function (e) {
                initFormSpecial();
            });

            function checkImage(filename) {
                var fileType = filename.split('.').pop().trim().toLowerCase();
                var validImageTypes = ["gif", "jpeg", "png", "jpg"];
                if ($.inArray(fileType, validImageTypes) < 0) {
                    alert('{{ __("web.invalid-image-type") }}');
                    return false;
                }
                return true;
            }

            $('input[name=image]').change(function (e) {
                var file = $(this).val();
                if (checkImage(file)) {
                    $('#preview_image').show();
                    $(this).parent().find('.custom-file-label').text(file);
                } else {
                    $(this).val('');
                }
            });
            $('#remove_image').click(function (e) {
                e.preventDefault();
                $('input[name=image]').val('');
                $('input[name=image]').parent().find('.custom-file-label').text('{{ __('web.choose-file') }}');
                $('#preview_image').hide();
            });
        });
</script>
@endpush
