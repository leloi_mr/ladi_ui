@extends('layouts.admin')

@section('title', __('food.index.title'))

@push('after-styles')
{{ Html::style('xtreme-admin/css/datatables.min.css') }}
{{ Html::style('xtreme-admin/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') }}
@endpush

@section('content')
{{ Breadcrumbs::render('food.index') }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <h3>{{ __('food.index') }}</h3>
                </div>
                <div class="col-6 text-right">
                    <a href="{{ route('food.create') }}" class="btn btn-primary">{{ __('web.create') }}</a>
                </div>
            </div>

            <div class="table-responsive mt-3">
                <table id="the_table" class="table table-striped table-bordered display wrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>{{ __('web.table.no') }}</th>
                            <th>{{ __('food.title') }}</th>
                            <th>{{ __('food.meal_kit_name') }}</th>
                            <th>{{ __('food.type') }}</th>
                            <th>{{ __('food.price_plus') }}</th>
                            <th>{{ __('food.is_in_week_menu') }}</th>
                            <th>{{ __('web.action') }}</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>

        </div>
    </div>
</div>
@endsection

@push('after-scripts')
{!! Html::script('xtreme-admin/js/datatables.min.js') !!}
{!! Html::script('xtreme-admin/assets/libs/bootstrap-switch/dist/js/bootstrap-switch.min.js') !!}
{!! Html::script('xtreme-admin/dist/js/app-style-switcher.js') !!}

<script>
    $(function () {
            "use strict";

            var table = $('#the_table').DataTable({
                dom: '<"pull-left"f><"pull-right"l>tip',
                processing: false,
                language: {
                    url: "{{ url('xtreme-admin/js/datatablelanguage.json') }}"
                },
                serverSide: true,
                ajax: {
                    url: "{{ route('food.index') }}?ajax"
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', width: '30px', className: 'text-center', searchable: false},
                    {data: 'title', name: 'title', width: '200px', className: 'text-center text-wrap width-200', render: function (data, type, row) {
                        return readMoreLess(data, 'less');
                    }},
                    {data: 'mealKitName', name: 'mealKitName', width: '120px', className: 'text-center'},
                    {data: 'typeName', name: 'typeName', width: '120px', className: 'text-center'},
                    {data: 'price_plus', name: 'price_plus', width: '120px', className: 'text-center'},
                    {data: 'is_in_week_menu_switch', name: 'is_in_week_menu_switch', width: '180px', orderable: false, className: 'px-0 text-center width-180', searchable: false},
                    {data: 'action', name: 'action', width: '150px', orderable: false, className: 'px-0 text-center', searchable: false},
                ],
                drawCallback: function (setting, json) {
                    setupReadMoreLessDatatable();
                    setupBtSwitchDatatable();
                }
            }).on('error.dt', function (e, settings, techNote, message) {
                console.log(e);
            });

            setupBtDeleteDatatable(table, '{{ __('food.index') }}');
        });
</script>
@endpush
