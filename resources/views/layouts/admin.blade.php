<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        @hasSection('title')
        @yield('title') - Arideli
        @else
        Arideli
        @endif
    </title>
    <meta name="description" content="@yield('meta_description', 'Laravel 5 Boilerplate')">
    <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">

    <link rel="icon" href="{{ url('/images/favicon.ico') }}">
    @yield('meta')

    @stack('before-styles')

    {{ Html::style('xtreme-admin/assets/libs/bootstrap/dist/css/bootstrap.min.css') }}
    {{ Html::style('xtreme-admin/dist/css/style.css') }}
    {{ Html::style('xtreme-admin/assets/libs/select2/dist/css/select2.min.css') }}
    {{-- try to get this resouce --}}
    {{ Html::style('xtreme-admin/assets/libs/toastr/toastr.min.css') }}
    {{ Html::style('xtreme-admin/assets/libs/sweetalert2/dist/sweetalert2.min.css') }}

    @stack('after-styles')

    {{ Html::style('css/mystyle.css') }}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->

    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light fixed-top">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ route('home') }}">
                        <!-- Logo icon -->
                        <b class="logo-text">
                            <!-- Light Logo icon -->
                            <img src="{{ url('images/logo.jpg') }}" alt="homepage" class="light-logo"
                                style="max-height: 58px;" />
                        </b>
                        <!--End Logo icon -->
                    </a>
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
                        data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
                            class="ti-menu ti-close"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <!-- left -->
                    <ul class="navbar-nav float-left mr-auto ml-2">
                        <li class="nav-item">
                            <a id="header-navbar-url-dashboard"
                                class="nav-link dropdown-toggle sidebar-link has-arrow waves-effect waves-dark"
                                href="{{ route('home') }}">
                                <span class="hide-menu">Dashboard</span></a>
                        </li>
                        <li class="nav-item">
                            <a id="header-navbar-url-booking"
                                class="nav-link dropdown-toggle sidebar-link has-arrow waves-effect waves-dark"
                                href="{{ route('food.index') }}">
                                <span class="hide-menu">{{ __('food.index')}}
                                </span></a>
                        </li>
                        <li class="nav-item">
                            <a id="header-navbar-url-booking"
                                class="nav-link dropdown-toggle sidebar-link has-arrow waves-effect waves-dark"
                                href="{{ route('mealkit.index') }}">
                                <span class="hide-menu">{{ __('mealkit.index')}}
                                </span></a>
                        </li>
                    </ul>

                    <!-- Right side toggle and nav items -->
                    <ul class="navbar-nav float-right align-content-center">
                        <li class="nav-item align-self-center">
                            <span
                                class="text-muted text-black-50">{{{ Auth::user()->getUsername() ? Auth::user()->getUsername() : '' }}}</span>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href=""
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img
                                    src="{{ asset('xtreme-admin/assets/images/users/1.jpg') }}" alt="user"
                                    class="rounded-circle" width="31"></a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off m-r-5 m-l-5"></i> {{ __('auth.logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            @if(Session::has('exception'))
            <div class="alert alert-danger">
                {{ Session::get('exception') }}
            </div>
            @endif
            @yield('content')
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->

    @stack('before-scripts')

    {!! Html::script('xtreme-admin/assets/libs/jquery/dist/jquery.min.js') !!}
    {!! Html::script('xtreme-admin/assets/libs/popper.js/dist/umd/popper.min.js') !!}
    {!! Html::script('xtreme-admin/assets/libs/bootstrap/dist/js/bootstrap.min.js') !!}

    {!! Html::script('xtreme-admin/dist/js/app.min.js') !!}
    {!! Html::script('xtreme-admin/dist/js/app.init.horizontal-fullwidth.js') !!}
    {!! Html::script('xtreme-admin/dist/js/app-style-switcher.horizontal.js') !!}

    {!! Html::script('xtreme-admin/dist/js/waves.js') !!}
    {!! Html::script('xtreme-admin/dist/js/sidebarmenu.js') !!}
    {!! Html::script('xtreme-admin/dist/js/custom.js') !!}
    {!! Html::script('xtreme-admin/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') !!}
    {!! Html::script('xtreme-admin/assets/extra-libs/sparkline/sparkline.js') !!}

    {!! Html::script('xtreme-admin/assets/libs/select2/dist/js/select2.min.js') !!}
    {!! Html::script('xtreme-admin/assets/libs/select2/dist/js/i18n/vi.js') !!}
    {!! Html::script('xtreme-admin/assets/libs/sweetalert2/dist/sweetalert2.min.js') !!}
    {!! Html::script('xtreme-admin/assets/libs/toastr/toastr.min.js') !!}
    {!! Html::script('xtreme-admin/assets/extra-libs/toastr/toastr-init.js') !!}

    {!! Html::script('js/myproject.js') !!}

    <script>
        function readMoreLess(text, status) {
            var res;
            if (text=="" || text.length <= 50) {
                res = text;
            } else {
                if (status == 'more') {
                    res = text + "<span class=\"morecontent\"><span>&nbsp;&nbsp;&nbsp;&nbsp;</span><a id=\"toggleButton\" class=\"read-more-less less\" data-text=\"" + text + "\" href=\"javascript:void(0);\">⇡●</a></span>";
                } else {
                    var shortText= text.substring(0,50);
                    var cleanStr= text.replace(/["']/g, "");
                    res = shortText + '...' + "<span class=\"morecontent\"><span>&nbsp;&nbsp;&nbsp;&nbsp;</span><a id=\"toggleButton\" class=\"read-more-less more\" data-text=\"" + text + "\" href=\"javascript:void(0);\">⇣●</a></span>";
                }
            }
            return '<div class="text-wrap" style="position: relative !important;">'+ res +'</div>';
        }

        function setupReadMoreLessDatatable(url, column, name) {
            $('.read-more-less').click(function (e) {
                var $e = $(e);
                var $link = $(e.currentTarget);
                var $text = $link.attr('data-text');
                var $divWrap = $link.parents('td');

                if ($link.hasClass('more')) {
                    $divWrap.html(readMoreLess($text, 'more'));
                } else {
                    $divWrap.html(readMoreLess($text, 'less'));
                }
                setupReadMoreLessDatatable(url, column, name);
            });
        }

        function setupBtSwitchDatatable(url, column, name) {
        //$(".bt-switch input[type='checkbox'], .bt-switch input[type='radio']").bootstrapSwitch();

        $(".bt-switch input[type='checkbox'], .bt-switch input[type='radio']").bootstrapSwitch({
            onSwitchChange: function(e, data) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var inp = $(e)[0].currentTarget;
                var $inp = $(inp).parents('.bt-switch');

                var url = $inp.data('remote');
                var column = $inp.data('column');
                var name = $inp.data('name');

                if (inp && url && column && name) {
                    var payload = {};
                    payload[column] = inp.checked;

                    $.ajax({
                        url: url,
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        data: payload,
                        success: function (data) {
                            if (data.status === 'success') {
                                toastr.success(convertMsg('{{ __('web.edit') }} ' + name + ' {{ __('web.success') }}'));
                            } else {
                                toastr.error(convertMsg('{{ __('web.edit') }} ' + name + ' {{ __('web.error') }}'));
                                console.log(data);
                            }
                        },
                        error: function (data) {
                            toastr.error(convertMsg('{{ __('web.edit') }} ' + name + ' {{ __('web.error') }}'));
                            console.log(data);
                        }
                    });
                } else {
                    toastr.error(convertMsg('{{ __('web.edit') }} ' + name + ' {{ __('web.error') }}'));
                    console.log($inp);
                }
            }
        });
        $(".bt-switch > input[type=checkbox]").width(200);
    }

    function setupBtDeleteDatatable(table, name) {
        $(document).on('click', '.btn-delete', function(e){
            e.preventDefault();

            swal.fire({
                title: '{{ __('web.confirm-delete') }}',
                text: '{{ __('web.confirm-delete-detail') }}',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ __('web.accept') }}',
                cancelButtonText: '{{ __('web.cancel') }}'
            }).then((result) => {
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    let url = $(this).data('remote');
                    let data = table.row($(this).parents('tr')).data();

                    if (data && data.hasOwnProperty('id')) {
                        $.ajax({
                            url: url,
                            type: 'DELETE',
                            dataType: 'json',
                            data: {id: data.id},
                            success: function (data) {
                                if (data.status === 'success') {
                                    table.draw(false);
                                    swal('{{ __('web.deleted') }}', convertMsg('{{ __('web.delete') }} '+ name +' {{ __('web.success') }}'), "success");
                                } else {
                                    swal('{{ __('web.cancelled') }}', convertMsg('{{ __('web.delete') }} '+ name +' {{ __('web.error') }}'), "error");
                                    console.log(data);
                                }
                            },
                            error: function (data) {
                                swal('{{ __('web.cancelled') }}', convertMsg('{{ __('web.delete') }} '+ name +' {{ __('web.error') }} {{ __('web.something-went-wrong') }}'), "error");
                                console.log(data);
                            }
                        });
                    }
                }
            });
        });
    }

    function listenValidateForm() {
        $('.custom-validate').change(function (e) {
            var $e = $(e.currentTarget);
            $e.val($e.val().trim());
            var element = $e.get(0);

            $(element).removeClass('is-invalid');
            var $error = $(element).parent().find('.text-validate');
            if ($error.length > 0) {
                $error.first().remove();
            }

            if ($e.hasClass('select2')) {
                $e.parent().find('.select2-selection--single').removeClass('has-error');
            }

            if (!element.checkValidity()) {
                var msg = getCustomMessage(element, element.validity);
                $(element).addClass('is-invalid');
                $(element).parent().append('<span class="invalid-feedback text-validate d-block" role="alert"><strong>' + msg + '</strong></span>');

                if ($e.hasClass('select2')) {
                    $e.parent().find('.select2-selection--single').addClass('has-error');
                }
            }
        });
    }

    function convertMsg(msg) {
        msg = msg.toLowerCase();
        msg = msg.charAt(0).toUpperCase() + msg.slice(1);

        return msg;
    }

    function addErrorValidateForm($e, msg) {
        msg = convertMsg(msg);
        $e.find('strong').text(msg);
        $e.removeClass('d-none').addClass('d-block').addClass('validate-after').show();
    }

    function fillMessage(element, key) {
        var nameShow = ($(element).attr('data-name-show') ? ' '+ $(element).attr('data-name-show') : $(element).attr('name') ? ' '+ $(element).attr('name') : '').toLowerCase();
        const customMessages = {
            badInput: '{!! __('validation.bad_input') !!}'.replace(':attribute', nameShow),
            rangeOverflow: '{!! __('validation.gt.numeric') !!}'.replace(':attribute', nameShow).replace(':value', $(element).attr('max') ? $(element).attr('max') : ''),
            rangeUnderflow: '{!! __('validation.lt.numeric') !!}'.replace(':attribute', nameShow).replace(':value', $(element).attr('min') ? $(element).attr('min') : ''),
            stepMismatch: '{!! __('validation.step_mismatch') !!}'.replace(':attribute', nameShow).replace(':value', $(element).attr('step') ? $(element).attr('step') : ''),
            tooLong: '{!! __('validation.gt.string') !!}'.replace(':attribute', nameShow).replace(':value', $(element).attr('maxlength') ? $(element).attr('maxlength') : ''),
            tooShort: '{!! __('validation.gt.string') !!}'.replace(':attribute', nameShow).replace(':value', $(element).attr('minlength') ? $(element).attr('minlength') : ''),
            valueMissing: '{!! __('validation.required') !!}'.replace(':attribute', nameShow),

            typeMismatch: '{!! __('validation.type_mismatch') !!}'.replace(':attribute', nameShow),
            emailMismatch: '{!! __('validation.email') !!}'.replace(':attribute', nameShow),
            patternMismatch: '{!! __('validation.regex') !!}'.replace(':attribute', nameShow),
        };
        var txt = customMessages[key];
        return txt;
    };

    function getCustomMessage(element, validity) {
        var nameShow = ($(element).attr('data-name-show') ? ' '+ $(element).attr('data-name-show') : $(element).attr('name') ? ' '+ $(element).attr('name') : '').toLowerCase();
        var txt = '{!! __('validation.bad_input') !!}'.replace(':attribute', nameShow);

        if (validity.typeMismatch) {
            txt = fillMessage(element, element.type +'Mismatch');
        } else {
            keys = ['valueMissing', 'tooLong', 'tooShort', 'rangeOverflow', 'rangeUnderflow', 'stepMismatch', 'badInput'];
            $.each(keys, function (i, e) {
                if (validity[e]) {
                    txt = fillMessage(element, e);
                    return false;
                }
            });
        }
        txt = convertMsg(txt);
        return txt;
    }

    function validateForm(submit) {
        var $form = $(submit.currentTarget).parents('form');

        $.each($form.find('.invalid-feedback:visible').not('.validate-after'), function (i, e) {
            $(e).find('strong').text('');
            $(e).removeClass('d-block').addClass('d-none').hide();
        });
        $.each($form.find('.validate-after'), function (i, e) {
            $(e).removeClass('validate-after');
        });
        $.each($form.find('.is-invalid'), function (i, e) {
            $(e).removeClass('is-invalid');
        });
        $.each($form.find('.has-error'), function (i, e) {
            $(e).removeClass('has-error');
        });

        $.each($form.find('.text-validate'), function (i, e) {
            $(e).remove();
        });

        $.each($form.find('.custom-validate'), function (i, e) {
            var element = $(e).get(0);
            if (element && !element.checkValidity()) {
                var msg = getCustomMessage(element, element.validity);
                //element.setCustomValidity(msg);
                //var msg = element.validationMessage;

                $(element).parents('.form-group').append('<span class="invalid-feedback text-validate d-block" role="alert"><strong>' + msg + '</strong></span>');
            }
        });
        $.each($form.find('.text-validate'), function (i, e) {
            $(e).show();
        });

        var $invalidElements = $form.find('.invalid-feedback:visible');
        if ($invalidElements.length > 0) {
            submit.preventDefault();

            $.each($invalidElements, function (i, e) {
                var $element = $(e).parents('.form-group').find('.custom-validate');
                $element.addClass('is-invalid');
                if ($element.hasClass('select2')) {
                    $element.parent().find('.select2-selection--single').addClass('has-error');
                }
            });

            $([document.documentElement, document.body]).animate({
                scrollTop: $invalidElements.first().offset().top - 200
            }, 200);
            $invalidElements.first().parents('.form-group').find('.custom-validate').focus();
            return false;
        }
        return true;
    }

    function initModal(table, txt) {
        $('.modal-create button[type=submit]').click(function (e) {
            if (!validateForm(e)) return false;
            e.preventDefault();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let $form = $('.modal-create form');
            let url = $form.attr('action');

            $.ajax({
                url: url + '?ajax',
                type: 'POST',
                dataType: 'json',
                data: $form.serializeArray(),
                success: function (data) {
                    if (data.status === 'success') {
                        $('.modal-create').modal('hide');
                        toastr.success(convertMsg("{{ __('web.create') }} "+ txt +" {{ __('web.success') }}"));
                    } else {
                        {{--toastr.error("{{ __('web.create') }} {{ __('web.error') }}");--}}

                        if (data.hasOwnProperty('validator')) {
                            var errors = data.validator;

                            for (var k in errors) {
                                var input = $('.modal-create input[name='+ k +']');
                                input.next().find('strong').html(errors[k][0]);
                                input.next().removeClass('d-none').show().addClass('d-block');
                            }
                        }
                    }
                },
            }).always(function (data) {
                table.draw(false);
            });
        });

        $(document).on('click', '.btn-edit', function(e){
            e.preventDefault();
            var name = $(this).data('name');
            $('.modal-edit form').attr('action', $(this).data('remote'));
            $('.modal-edit form input[name=name]').val(name);
            $('.modal-edit').modal('show');
        });

        $('.modal-edit button[type=submit]').click(function (e) {
            if (!validateForm(e)) return false;
            e.preventDefault();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let $form = $('.modal-edit form');
            let url = $form.attr('action');

            $.ajax({
                url: url + '?ajax',
                type: 'PUT',
                dataType: 'json',
                data: $form.serializeArray(),
                success: function (data) {
                    if (data.status === 'success') {
                        $('.modal-edit').modal('hide');
                        toastr.success(convertMsg("{{ __('web.edit') }} "+ txt +" {{ __('web.success') }}"));
                    } else {
                        if (data.hasOwnProperty('validator')) {
                            var errors = data.validator;

                            for (var k in errors) {
                                var input = $('.modal-edit input[name='+ k +']');
                                input.next().find('strong').html(errors[k][0]);
                                input.next().removeClass('d-none').show().addClass('d-block');
                            }
                        }
                    }
                },
            }).always(function (data) {
                table.draw(false);
            });
        });
    }

    $(document).ready(function() {
        $('#navbarDropdownCatalog').hover(function(e){
            $('#navbarDropdownCatalog').parent().find('.dropdown-menu').show();
        }, function(e){
            $('#navbarDropdownCatalog').parent().find('.dropdown-menu').hide();

            $('#navbarDropdownCatalog').parent().find('.dropdown-menu').hover(function(e){
                $('#navbarDropdownCatalog').parent().find('.dropdown-menu').show();
            }, function(e){
                $('#navbarDropdownCatalog').parent().find('.dropdown-menu').hide();
            });
        });

        $('#navbarDropdownApplyCruise').hover(function(e){
            $('#navbarDropdownApplyCruise').parent().find('.dropdown-menu').show();
        }, function(e){
            $('#navbarDropdownApplyCruise').parent().find('.dropdown-menu').hide();

            $('#navbarDropdownApplyCruise').parent().find('.dropdown-menu').hover(function(e){
                $('#navbarDropdownApplyCruise').parent().find('.dropdown-menu').show();
            }, function(e){
                $('#navbarDropdownApplyCruise').parent().find('.dropdown-menu').hide();
            });
        });

        $('#navbarDropdownConfig').hover(function(e){
            $('#navbarDropdownConfig').parent().find('.dropdown-menu').show();
        }, function(e){
            $('#navbarDropdownConfig').parent().find('.dropdown-menu').hide();

            $('#navbarDropdownConfig').parent().find('.dropdown-menu').hover(function(e){
                $('#navbarDropdownConfig').parent().find('.dropdown-menu').show();
            }, function(e){
                $('#navbarDropdownConfig').parent().find('.dropdown-menu').hide();
            });
        });

        $('.modal').on('hidden.bs.modal', function (e) {
            $.each($('.is-invalid'), function (i, e) {
                $(e).removeClass('is-invalid');
            });
            $.each($('span.invalid-feedback'), function (i, e) {
                $(e).removeClass('d-block').hide().addClass('d-none');
                $(e).find('strong').html('');
            });
            $(this)
                .find("input,textarea,select")
                .val('')
                .end()
                .find("input[type=checkbox], input[type=radio]")
                .prop("checked", "")
                .end();
        });


        @if(Session::has('success'))
            var msg = convertMsg('{{ Session::get('success') }}');

            toastr.success(msg);
        @endif

        @if(Session::has('info'))
            var msg = convertMsg('{{ Session::get('info') }}');

            toastr.info(msg);
        @endif

        @if(Session::has('primary'))
            var msg = convertMsg('{{ Session::get('primary') }}');

            toastr.primary(msg);
        @endif

        @if(Session::has('danger'))
            var msg = convertMsg('{{ Session::get('danger') }}');

            toastr.danger(msg);
        @endif

        @if(Session::has('warning'))
            var msg = convertMsg('{{ Session::get('warning') }}');

            toastr.warning(msg);
        @endif

        @if(Session::has('error'))
            var msg = convertMsg('{{ Session::get('error') }}');

            toastr.error(msg);
        @endif

    });
    </script>

    @stack('after-scripts')

</body>


</html>
