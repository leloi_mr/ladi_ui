@extends('layouts.admin')

@section('title', __('web.editing', ['name' => __('mealkit.index')]))

@push('after-styles')
{{ Html::style('xtreme-admin/assets/libs/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') }}
<style>
    #preview_image,
    #preview_image_seo {
        max-width: 150px;
        height: auto;
    }
</style>
@endpush

@section('content')
{{ Breadcrumbs::render('mealkit.edit') }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body mx-5">
            <form class="mt-3" action="{{ route('mealkit.update', ['id' => $mealkit->id]) }}" method="post"
                enctype="multipart/form-data" id="the_form">
                <input type="hidden" name="_method" value="PUT">
                @csrf

                <h3 class="card-title text-center">{{ __('web.editing', ['name' => __('mealkit.index')]) }}</h3>
                <div class="form-body">
                    <div class="card-body pt-0">
                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="title">{{ __('mealkit.title') }} <span
                                            class="text-danger">(*)</span></label>
                                    <input id="title" type="text" data-name-show="{{ __('mealkit.title') }}"
                                        class="custom-validate form-control form-control-lg @error('title') is-invalid @enderror"
                                        name="title" value="{{ old('title') ? old('title') : $mealkit->title }}" autofocus
                                        maxlength="255" required>

                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="alias">{{ __('mealkit.alias') }} {{ __('mealkit.alias_suggest') }}<span
                                            class="text-danger">(*)</span></label>
                                    <input id="alias" type="text" data-name-show="{{ __('mealkit.alias') }}"
                                        class="custom-validate form-control form-control-lg @error('alias') is-invalid @enderror"
                                        name="alias" value="{{ old('alias') ? old('alias') : $mealkit->alias }}" autofocus
                                        maxlength="255" required>

                                    @error('alias')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="image" class="control-label">{{ __('mealkit.image') }}
                                        {{ __('mealkit.image_size_suggest') }}</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">{{ __('web.upload') }}</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" accept="image/*"
                                                class="custom-file-input form-control form-control-lg @error('image') is-invalid @enderror"
                                                name="image" value=""
                                                onchange="document.getElementById('output_image').src = window.URL.createObjectURL(this.files[0]);">
                                            <label class="custom-file-label" for="inputGroupFile01">{{ __('web.choose-file') }}</label>
                                            <input type="hidden" name="is_no_action_image" value="true"
                                                id="is_no_action_image">
                                        </div>
                                    </div>
                                    <div class="card mt-2" id="preview_image">
                                        <img id="output_image" alt="{{ __('mealkit.image') }}"
                                            class="img-thumbnail"
                                            src="{{ $mealkit->image_url ? $mealkit->image_url : '' }}" />
                                        <div class="card-action position-relative">
                                            <button class="btn btn-danger position-absolute"
                                                style="bottom: 10px; right: 10px;" id="remove_image"><i
                                                    class="ti-trash"></i></button>
                                        </div>
                                    </div>
                                    @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="description">{{ __('mealkit.description') }}</label>
                                    <textarea id="description" data-name-show="{{ __('mealkit.description') }}"
                                        class="custom-validate form-control form-control-lg @error('description') is-invalid @enderror"
                                        name="description" maxlength="255"
                                        >{{ old('description') ? old('description') : $mealkit->description }}</textarea>

                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="invalid-feedback d-none" id="validate_description" role="alert">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-6" id="form_type">
                                <div class="form-group">
                                    <label class="control-label" for="type">{{ __('mealkit.type') }} <span
                                            class="text-danger">(*)</span></label>
                                    <select name="type" id="type" data-name-show="{{ __('mealkit.type') }}"
                                        class="select2 custom-validate form-control form-control-lg @error('type') is-invalid @enderror"
                                        required>
                                        <option value="" disabled selected>{{ __('web.select-an-option') }}</option>

                                        @foreach($listTypeArray as $typeArray)
                                        <option value="{{ $typeArray['value'] }}"
                                            {{ old('type') && old('type') == $typeArray['value'] ? ' selected' : $mealkit->type == $typeArray['value'] ? ' selected' : '' }}>
                                            {{ $typeArray['name'] }}
                                        </option>
                                        @endforeach
                                    </select>

                                    @error('type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>                        
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="order">{{ __('mealkit.order') }} <span
                                            class="text-danger">(*)</span></label>
                                    <div class="row">
                                        <input id="order" type="number"
                                            data-name-show="{{ __('mealkit.order') }}"
                                            class="col-md-4 custom-validate form-control form-control-lg @error('order') is-invalid @enderror"
                                            name="order"
                                            value="{{ old('order') ? old('order') : $mealkit->order ? old('order') ? old('order') : $mealkit->order : 0 }}"
                                            autofocus min="0" required>
                                    </div>

                                    @error('order')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="card-body text-center">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>
                                    {{ __('web.save') }}</button>
                            </div>
                        </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('after-scripts')
{!! Html::script('xtreme-admin/assets/libs/ckeditor/ckeditor.js') !!}

<script>
    $(document).ready(function () {
            CKEDITOR.config.filebrowserImageUploadUrl = '{!! route("ckEditorUploadPhoto")."?_token=".csrf_token() !!}';
            CKEDITOR.config.height = '300px';
            CKEDITOR.replace(document.querySelector( '#description' ));

            $.each($('.select2'), function (i, e) {
                $(e).select2({
                    placeholder: '{{ __("web.select-an-option") }}'
                });
            });

            $('#preview_image').hide();
            if ($('#output_image').attr('src') != '') $('#preview_image').show();

            listenValidateForm();
            $('button[type=submit]').click(function (e) {
                // if (CKEDITOR.instances.description.getData() == '') {
                //     var msg = '{!! __('validation.required', ['attribute' => __('mealkit.description')]) !!}';
                //     addErrorValidateForm($('#validate_description'), msg);
                // }

                validateForm(e);
            });

            function checkImage(filename) {
                var fileType = filename.split('.').pop().trim().toLowerCase();
                var validImageTypes = ["gif", "jpeg", "png", "jpg"];
                if ($.inArray(fileType, validImageTypes) < 0) {
                    alert('{{ __("web.invalid-image-type") }}');
                    return false;
                }
                return true;
            }

            $('input[name=image]').change(function (e) {
                var file = $(this).val();
                if (checkImage(file)) {
                    $('#preview_image').show();
                    $(this).next('.custom-file-label').text(file);
                } else {
                    $(this).val('');
                }
            });
            $('#remove_image').click(function (e) {
                e.preventDefault();
                $('#is_no_action_image').val('false');
                $('input[name=image]').val('');
                $('input[name=image]').parent().find('.custom-file-label').text('{{ __('web.choose-file') }}');
                $('#preview_image').hide();
            });
        });
</script>
@endpush
