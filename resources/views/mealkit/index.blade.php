@extends('layouts.admin')

@section('title', __('mealkit.index.title'))

@push('after-styles')
{{ Html::style('xtreme-admin/css/datatables.min.css') }}
@endpush

@section('content')
{{ Breadcrumbs::render('mealkit.index') }}

<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <h3>{{ __('mealkit.index') }}</h3>
                </div>
            </div>

            <div class="table-responsive mt-3">
                <table id="the_table" class="table table-striped table-bordered display wrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>{{ __('web.table.no') }}</th>
                            <th>{{ __('mealkit.title') }}</th>
                            <th>{{ __('mealkit.alias') }}</th>
                            <th>{{ __('mealkit.order') }}</th>
                            <th>{{ __('mealkit.type') }}</th>
                            <th>{{ __('web.action') }}</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>

        </div>
    </div>
</div>
@endsection

@push('after-scripts')
{!! Html::script('xtreme-admin/js/datatables.min.js') !!}

<script>
    $(function () {
            "use strict";

            var table = $('#the_table').DataTable({
                dom: '<"pull-left"f><"pull-right"l>tip',
                processing: false,
                language: {
                    url: "{{ url('xtreme-admin/js/datatablelanguage.json') }}"
                },
                serverSide: true,
                ajax: {
                    url: "{{ route('mealkit.index') }}?ajax"
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', width: '30px', className: 'text-center', searchable: false},
                    {data: 'title', name: 'title', width: '160px', className: 'text-center', render: function (data, type, row) {
                        return readMoreLess(data, 'less');
                    }},
                    {data: 'alias', name: 'alias', width: '160px', className: 'text-center', render: function (data, type, row) {
                        return readMoreLess(data, 'less');
                    }},
                    {data: 'order', name: 'order', width: '120px', className: 'text-center'},
                    {data: 'typeName', name: 'typeName', width: '120px', className: 'text-center'},
                    {data: 'action', name: 'action', width: '150px', orderable: false, className: 'px-0 text-center', searchable: false},
                ],
                drawCallback: function (setting, json) {
                    setupReadMoreLessDatatable();
                }
            }).on('error.dt', function (e, settings, techNote, message) {
                console.log(e);
            });

            // setupBtDeleteDatatable(table, '{{ __('mealkit.index') }}');
        });
</script>
@endpush
