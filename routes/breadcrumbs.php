<?php

// Dashboard
Breadcrumbs::for('dashboard', function ($trail) {
    $trail->push('Dashboard', route('home'));
});

// Dashboard > Food
Breadcrumbs::for('food.index', function ($trail) {
    $trail->parent('dashboard');
    $trail->push(__('food.index'), route('food.index'));
});

// Dashboard > Food
Breadcrumbs::for('food.create', function ($trail) {
    $trail->parent('food.index');
    $trail->push(__('web.creating', ['name' => __('food.index')]), route('food.index'));
});

// Dashboard > Food > Edit
Breadcrumbs::for('food.edit', function ($trail) {
    $trail->parent('food.index');
    $trail->push(__('web.editing', ['name' => __('food.index')]), route('food.index'));
});

// Dashboard > Mealkit
Breadcrumbs::for('mealkit.index', function ($trail) {
    $trail->parent('dashboard');
    $trail->push(__('mealkit.index'), route('mealkit.index'));
});

// Dashboard > Mealkit
Breadcrumbs::for('mealkit.create', function ($trail) {
    $trail->parent('mealkit.index');
    $trail->push(__('web.creating', ['name' => __('mealkit.index')]), route('mealkit.index'));
});

// Dashboard > Mealkit > Edit
Breadcrumbs::for('mealkit.edit', function ($trail) {
    $trail->parent('mealkit.index');
    $trail->push(__('web.editing', ['name' => __('mealkit.index')]), route('mealkit.index'));
});
