<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AuthController@index')->name('welcome');
Route::get('/', 'AuthController@index')->name('login');

Route::get('/logout', function () {
    return __('auth.logout');
})->name('logout');

Route::middleware('auth:web')
    ->group(function () {
        Route::get('/home', 'HomeController@index')->name('home');

        // Food
        Route::get('/food', 'FoodController@index')->name('food.index');
        Route::get('/food/create', 'FoodController@create')->name('food.create');
        Route::post('/food', 'FoodController@store')->name('food.store');
        Route::get('/food/{id}', 'FoodController@edit')->name('food.edit');
        Route::put('/food/{id}', 'FoodController@update')->name('food.update');
        Route::delete('/food/{id}', 'FoodController@destroy')->name('food.destroy');
        Route::post('/food/{id}', 'FoodController@isInWeekMenu')->name('food.is_in_week_menu');
        
        // Mealkit
        Route::get('/mealkit', 'MealkitController@index')->name('mealkit.index');
        Route::get('/mealkit/{id}', 'MealkitController@edit')->name('mealkit.edit');
        Route::put('/mealkit/{id}', 'MealkitController@update')->name('mealkit.update');

        Route::get('/recipes', function () {
            return 'Preparing';
        })->name('recipes.index');

        Route::post('ckEditorUploadPhoto', 'MediaController@uploadPhoto')->name('ckEditorUploadPhoto');
    });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
